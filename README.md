This is an implementation of the Bayesian Causal Inference of Body Ownership model by Samad et al. (2015).

## How to use
To reproduce the reported results first install the virtual environment through

`conda env create -f environment.yml`

**or**

`pip install -r requirements.txt`

Then run

`python main.py` 

## Folder Structure
- derivation/ contains PDF files that try to explain the derivation of the mathematical formulas contained in the `__comIntegral` and `__septIntegral` functions in bcibo_class.py
  - **These documents were written for internal use and were not originally intended for publication!** If anything in them is unclear to you, feel free to contact Moritz Schubert (for e-mail address see `git log`)
- output/ contains the exact results (in csv form) reported and the figures presented in Schubert and Endres (in press)

## References
Samad, Majed, Albert Jin Chung, and Ladan Shams. 2015. “Perception of Body Ownership Is Driven by Bayesian Sensory Inference.” Edited by Mikhail A. Lebedev. PLOS ONE 10 (2). https://doi.org/10.1371/journal.pone.0117178.

#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
main script
"""

# standard library
import os
import pathlib
import pickle
from typing import Union
import warnings

# external
import frictionless as fl
import matplotlib.pyplot as plt  # type: ignore
import numpy as np
import pandas as pd
from tqdm import tqdm

# local
from bcibo_class import BCIBO_Model


class Col():
    """names of the csv columns"""

    DISTANCE = "distance"
    MEAN = "p(H1)_mean"
    MEAN_ROUND = "p(H1)_mean_round3"
    MEAN_UP = "p(H1)_mean_round3up"
    SEM = "p(H1)_sem"
    SEM_UP = "p(H1)_sem_round3up"
    SIGMA = "sigma_exponent"


def floor_or_ceiling(number:float, decimals:int=2):
    """
    Returns floor for negative and ceiling for positive values.

    The idea is to be able to give statements like "all values were under <value>"

    Code adapted from https://kodify.net/python/math/round-decimals/
    License terms: https://kodify.net/terms-privacy/#copyright-of-code-examples
    """

    if number <= 0:
        limit = np.floor
    elif number > 0:
        limit = np.ceil

    if not isinstance(decimals, int):
        raise TypeError("decimal places must be an integer")
    elif decimals < 0:
        raise ValueError("decimal places has to be 0 or more")
    elif decimals == 0:
        return limit(number)

    factor = 10 ** decimals
    return limit(number * factor) / factor


def diff_between_dfs(df1, name1, df2, name2,
                     outputDir='output'):

    # difference between the models for Samad et al.'s parameter choice
    df_diff = df1.loc[:, ['sigma_exponent', 'distance']]

    pH1_df1 = df1.loc[:, 'p(H1)_mean']
    pH1_df2 = df2.loc[:, 'p(H1)_mean']
    diff = pH1_df2 - pH1_df1
    df_diff['diff'] = diff.values
    for i in df_diff.index:
        df_diff.loc[i, 'diff_round3up'] = floor_or_ceiling(diff.loc[i], 3)
    df_diff = df_diff.reset_index(drop=True)

    df_diff.to_csv(pathlib.Path(outputDir, f'diff_{name1}-{name2}.csv'),
                   index=False)

    return df_diff


def distance_simulation(fname,
                        input_data: Union[None, pd.core.frame.DataFrame] = None,
                        outputDir: str = 'output',
                        distances: range = range(160, 360+1, 20),
                        exponents: range = range(0, 35+1, 5),
                        fig_title: Union[None, str, float] = None,
                        show_legend: bool = True,
                        **kwargs):

    if input_data is None:
        positionReal = 320 # Real: real hand

        # creates 2D array with hierarchical structure, sigma_exponent is the
        #  higher level
        df = pd.DataFrame({'sigma_exponent': np.repeat(exponents, len(distances)),
                           'distance': np.tile(distances, len(exponents))})

        # tqdm() simply creates a nice looking progress bar
        for distance in tqdm(distances, desc=fname.ljust(17)):
            positionRub = positionReal - distance # Rub: rubber hand

            for exponent in exponents:
                sigma = 10 ** exponent
                model = BCIBO_Model(xv_mu = positionRub,
                                    sigma = sigma,
                                    **kwargs)

                df.loc[(df.sigma_exponent == exponent) & (df.distance == distance),
                       'p(H1)_mean'] = model.post_c1 #posterior probability of C_1
                df.loc[(df.sigma_exponent == exponent) & (df.distance == distance),
                       'p(H1)_mean_round3'] = round(model.post_c1, 3)
                df.loc[(df.sigma_exponent == exponent) & (df.distance == distance),
                       'p(H1)_mean_round3up'] = floor_or_ceiling(model.post_c1, 3)
                df.loc[(df.sigma_exponent == exponent) & (df.distance == distance),
                        'p(H1)_sem'] = model.sem_c1
                # round up, so that we can say that all values are below a certain
                #  threshold
                df.loc[(df.sigma_exponent == exponent) & (df.distance == distance),
                        'p(H1)_sem_round3up'] = floor_or_ceiling(model.sem_c1, 3)

        fpath = pathlib.Path(outputDir, f'{fname}.csv')
        df.to_csv(fpath, index=False)
        distance_resource(fpath)
    else:
        df = input_data

    fig, ax = plot_distance(fname, df,
                            outputDir,
                            show_legend=show_legend,
                            fig_title=fig_title,
                            **kwargs)

    fig.savefig(
        pathlib.Path(outputDir, f"{fname}.png"),
        dpi=200, bbox_inches='tight',
        )

    fig.savefig(
        pathlib.Path(outputDir, f"{fname}.pdf"),
        bbox_inches='tight',
        )

    return df


def distance_resource(fpath: pathlib.Path):

    resource = fl.Resource(fpath)
    resource.infer()

    # sigma_exponent and distance inferred as integer, but could in principle be a number
    for field_name in [Col.DISTANCE, Col.SIGMA]:
        field = [field for field in resource.schema.fields if field['name']==field_name][0]
        field.type = 'number'

    resource.licenses = [{'name': "CC0-1.0",
                         "title": "CC0 1.0 Universal",
                         "path": "https://creativecommons.org/publicdomain/zero/1.0/"}]

    fname = fpath.stem
    description = "BCIBO: Bayesian Causal Inference of Body Ownership\n"

    if fname == "original":
        title = 'Original BCIBO model'
        description += ("implemented in accordance with Samad et al. (2015, "
                        "DOI: 10.1371/journal.pone.0117178)")
    elif fname == "non-truncated":
        title = 'Original BCIBO model with distance values shifted to the left'
        description += ("implemented mostly in accordance with Samad et al. (2015, "
                        "DOI: 10.1371/journal.pone.0117178)\n"
                        "only difference is that the distance values are "
                        "[20, 40, 60, ..., 220]")
    elif fname == "truncated":
        title = "Truncated BCIBO model"
        description += ("sensory priors were truncated to sensible lengths implemented "
                        "as described in Schubert and Endres (in review)")
    elif fname == "99percent":
        title = "BCIBO model with prior probability of C=1 set to 0.99"
        description += ("implemented mostly in accordance with Samad et al. (2015, "
                        "DOI: 10.1371/journal.pone.0117178)\n"
                        "only difference is that the  prior for the common cause "
                        "hypothesis was set to 0.99")
    elif fname == "humanScale":
        title = "BCIBO model with width of sensory priors set to 10^4 mm or ms"
        description += ("implemented mostly in accordance with Samad et al. (2015, "
                        "DOI: 10.1371/journal.pone.0117178)\n"
                        "only difference is that the sensory priors' standard "
                        "deviation was set to 10^4 mm or ms to use a standard "
                        "deviation that is roughly on a human scale")
    elif fname == "closeTo100percent":
        title = "BCIBO model with prior probability of C=1 set to 1-10^(-16)"
        description += ("implemented mostly in accordance with Samad et al. (2015, "
                        "DOI: 10.1371/journal.pone.0117178)\n"
                        "only difference is that the prior for the common cause "
                        "hypothesis was set to 1-10^(-16) mm or ms")
    else:
        warnings.warn(f"title of Frictionless resource could not be set, because '{fname}' is an unknown file name")
        title = fname
        description = 'n/a'

    resource.title = title
    resource.description = description
    # path needs to be in relation to the csv file, not its parent directory
    resource.path = fpath.name

    # description for csv column names

    for field in resource.schema.fields:
        name = field['name']
        if name == Col.DISTANCE:
            field.title = "distance in millimeter from the real hand"
        elif name == Col.MEAN:
            field.title = "average posterior probability of H1"
        elif name == Col.MEAN_ROUND:
            field.title = "average posterior probability of H1 rounded to the third digit"
        elif name == Col.MEAN_UP:
            field.title = "average posterior probability of H1 rounded up to the third digit"
            field.description = "This is helpful to quickly see whether the value is below a certain threshold (e.g. 0.05)"
        elif name == Col.SEM:
            field.title = "standard error of the posterior probability of H1"
        elif name == Col.SEM_UP:
            field.title = "standard error of the posterior probability of H1 rounded up to the third digit"
            field.description = "This is helpful to quickly see whether the value is below a certain threshold (e.g. 0.05)"
        elif name == Col.SIGMA:
            field.title = "exponent of the prior's standard deviation"
            field.description = "The standard deviation of the prior is 10 to the power of sigma_exponent."
        else:
            raise ValueError(("csv file contains 'unregistered' column name and thus "
                             "can't be documented"))


    resource.to_yaml(pathlib.Path(fpath.parent, fpath.stem + '.resource.yaml'))


    return resource


def mdpi_computers(outputDir: str = 'output'):
    """
    data analysis used to produce the results presented in Schubert and Endres'
    contribution to MDPI Computers' special issue "Advances in Seated Virtual
    Reality"

    Parameters
    ----------
    outputDir : str, optional
        The directory into which to save the csv tables and plots.
        The default is 'output'.

    Returns
    -------
    None.

    """

    # use the seed used in all of the results from Schubert and Endres (in press)
    file = open('randomseed.pkl', 'rb')
    seed = pickle.load(file)
    file.close()

    # uncomment to use random seed for data sampling
    # seed = None

    if not os.path.exists(outputDir):
        os.mkdir(outputDir)

    # Figure 3 (right)
    df_original = distance_simulation('original', seed=seed)

    shifted_distances = range(20, 220+1, 20)
    # original model, Figure 4 (left)
    distance_simulation('non-truncated', seed=seed,
                        fig_title='original',
                        distances=shifted_distances,
                        bbox_to_anchor=(.13,.37), # placement of plot legend
                        loc='lower left', # placement of plot legend
                        )

    # truncated model, Figure 4 (right)
    distance_simulation('truncated', seed=seed,
                        distances=shifted_distances,
                        truncated=True,
                        fig_title='truncated')

    # Figure 5 (left)
    distance_simulation('50percent', seed=seed,
                        fig_title=.5,
                        input_data=df_original)

    # Figure 5 (right)
    df_p99 = distance_simulation('99percent', seed=seed,
                                  fig_title=.99,
                                  hyperprior=.99)

    # sigma values for priors on human scale
    distance_simulation('humanScale', seed=seed,
                        exponents = [4])

    # hyperprior=1-10**-16
    distance_simulation('closeTo100percent',
                        seed=seed,
                        hyperprior=1-10**-16)

    # difference between original and hyperprior=-.99 model
    diff_between_dfs(df_original, 'original',
                      df_p99, '99percent',
                      outputDir=outputDir)

    package = fl.Package()
    yaml_files = [file for file in os.scandir(outputDir) if file.name[-4:]=='yaml']
    for yaml_file in yaml_files:
        resource = fl.Resource(yaml_file.path)
        package.add_resource(resource)

def plot_distance(
        fname: str,
        df: pd.core.frame.DataFrame,
        outputDir: str = 'output/',
        fig_title: Union[None, str, float] = None,
        show_legend: bool = True,
        **kwargs
) -> None:
    """
    analyse and visualise model for different distancces between real and rubber hand

    Returns
    -------
    None
        only side effects through output of plots

    """
    x_distance = df['distance'].unique()
    exponents =  df['sigma_exponent'].unique()

    # distance plotting
    fig, ax = plt.subplots(figsize=[5.5, 5.1])
    # make figures colorblind-friendly
    plt.style.use('seaborn-colorblind')
    for i, exponent in enumerate(exponents):
        y_prob = df.loc[df.sigma_exponent==exponent, 'p(H1)_mean'].values
        exponent_str = str(exponent)
        ax.plot(
            x_distance, y_prob, "x-", label=f"$10^{{{exponent_str}}}$",
            zorder=9 - i
        )

    ax.set_xticks(x_distance)
    ax.set_yticks(np.arange(0, 1+.1, .2))
    ax.set_xlabel("distance between real and rubber hand (mm)")
    ax.set_ylabel("posterior probability of $C_1$")

    if type(fig_title) == float:
        ax.set_title(f'$p(C_1) = {fig_title}$')
    elif fig_title:
        ax.set_title(fig_title)

    ax.grid(alpha=0.5, zorder=0)

    if 'bbox_to_anchor' in kwargs and 'loc' in kwargs:
        fig.legend(title="$\sigma$ (mm|ms)", loc=kwargs["loc"],
                   bbox_to_anchor=kwargs['bbox_to_anchor']
                   )
    else:
        fig.legend(title='$\sigma$ (mm|ms)', loc='upper right',
                   bbox_to_anchor=(.85, .85))

    return fig, ax



#%% main
if __name__ == "__main__":

    mdpi_computers()

#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
from scipy import special, stats
from typing import Literal, Union


class Observable:
    """observable parameters of the model

    i.e. the data used to update the prior

    Attributes
    ----------
    dist : stats._distn_infrastructure.rv_frozen
        generative function of the data, i.e. distribution from which the data
        is drawn
    mean : np.float64
        mean of the data generating distribution, in case of the normal
        distribution this is the mu parameter
    std : np.float64
        standard deviation of the generative function, sigma parameter in case
        of normal distribution

    Methods
    -------

    draw_sample(n)
        draw n samples from the data generating distribution
    """

    def __init__(
        self,
        mu: float,
        sigma: float,
    ) -> None:
        """
        Parameters
        ----------
        dist : stats._distn_infrastructure.rv_frozen
            distribution that describes the data generating processies of forced captures and can be played from memory by most average players.[a] The other openings: 1.a3, 1.b4, 1.c3, 1.f3, 1.f4, 1.h3, 1.h4, 1.Na3, 1.Nc3, 1.Nf3 can be solved by very skilled and exper
        """
        self.dist = stats.norm(mu, sigma)
        self.mu = mu
        self.sigma = sigma

    def draw_sample(self, n: int) -> None:
        """draw a sample of size n from the parameter distributions

        Parameters
        ----------
        n : int
            sample size

        Returns
        -------
        self.sample :
        """
        self.sample = self.dist.rvs(n)

        return self.sample


class BCIBO_Model:
    """instance of the BCIBO model (Samad et al., 2015)

    BCIBO stands for Bayesian Causal Inference of Body Ownership
    The main task of this class is to compute the posterior probability of the
    illusion occurring for n trials of a rubber hand illusion experiment. The
    mean of the likelihoods and posterior probabilities is computed as a
    summary statistic.
    
    The default values of the model parameters are in accordance with Samad et
    al. (2015)

    Parameters
    ----------
    < >_mu : float, optional
        mu parameter of the normal likelihood function of one of the following
        sensory modalities:
            xv: spatial visual input caused by the rubber hand
                The default is 160.
            xp: (spatial) proprioceptive input caused by the real hand
                The default is 320.
            tv: temporal visual input caused by the brush strokes on the
                rubber hand
                The default is 0.
            tt: (temporal) tactile input caused by the brush strokes on the
                real hand
                The default is 0.
    < >_sigma : float, optional
        sigma parameter of the normal likelihood functions of one of the 
        sensory modalities outlined above
    sigma : float, optional
        standard deviation of both the spatial and temporal priors
        The default is 10 ** 35.
    n : int, optional
        number of samples drawn for the fake data.
        The default is 10_000.
    synch : bool, optional
        whether to simulate a trial with synchronous (True) or asynchronous (False)
        stroking.
        The default is True.
    
    Attributes
    ----------
    xv/xp : Observable
        likelihood of the spatial (x) visual (v) or proprioceptive (p) input
    tv/tt : Observable
        likelihood of the temporal (t) visual (v) or tactile (t) input
    h1_< >_sigma : float
        sigma parameter of the normal prior for the spatial (x) or temporal (t)
        sensory data under H_1. H_1 denotes the common cause hypothesis, i.e. the
        hypothesis under which the effect (illusion) occurs
    h0_< >_sigma : float
        sigma parameter of the normal prior for the spatial visual (xv), spatial
        proprioceptive (xp), temporal visual (tv) and temporal tactile (tt) sensory
        data under H_0. H_0 denotes the separate causes hypothesis, i.e. the null
        hypothesis (called like this, because the illusion does not occur)
    like< > : float
        mean of the trials' likelihoods, both under the common cause (1)
        and the separate causes (0) hypotheses
    p0/p1 : float
        mean of the trials' posterior probabilities, both under the common
        causes (p1) and the separate causes (p0) hypothesis
    
    Methods
    -------
    infer()
        compute the posterior probability for each set of sensory input in the sample
        Said posterior is stored in the attributes p0/p1 and the likelihoods are
        stored in like< >
    
    Bound values
    ------------
    chiV : 2m --> 2_000 mm
            abitrary size of the room (4m length)
    chiP : 85 cm --> 850 mm
            rough estimate of average human height: 1,70 m
    tauV/tauT : 5 min --> 300_000 ms
            abitrary length until to expect the first brush stroke
    
    References
    ----------
    Samad, M., Chung, A. J., & Shams, L. (2015). Perception of Body Ownership
        Is Driven by Bayesian Sensory Inference. PLOS ONE, 10(2), 1–23.
        https://doi.org/10.1371/journal.pone.0117178
    
    Examples
    --------
    >>> model = BCIBO_Model()
    >>> print(model.post_c1)
    1.0
    """

    def __init__(
        self,
        xv_mu: float = 160.,
        xv_sigma: float = 1.,
        xp_mu: float = 320.,
        xp_sigma: float = 15.,
        tv_mu: float = 0.,
        tv_sigma: float = 20.,
        tt_mu: float = 0.,
        tt_sigma: float = 20.,
        sigma: float = 10. ** 35,
        hyperprior: float = .5,
        n: int = 100_000,
        synch: bool = True,
        truncated: bool = False,
        bound_chiV: float = 2_000.,
        bound_chiP: float = 850,
        bound_tau: float = 3_600_000,
        seed: Union[None, tuple] = None,
        **kwargs
    ) -> None:

        self.xv = Observable(xv_mu, xv_sigma)
        self.xp = Observable(xp_mu, xp_sigma)
        if synch:
            self.tv = Observable(tv_mu, tv_sigma)
        else:
            self.tv = Observable(tv_mu + 600, tv_sigma)
        self.tt = Observable(tt_mu, tt_sigma)
        self.params = [self.xv, self.xp, self.tv, self.tt]
        
        self.sigma = sigma
        self.hyperprior = hyperprior
        self.n = n
        
        self.truncated = truncated
        self.bound_chiV = bound_chiV
        self.bound_chiP = bound_chiP
        self.bound_tau = bound_tau
        
        self.seed = seed

        self.infer()

    def __sepIntegral(self,
                      observable: Observable,
                      sigma: float,
                      dimension: Literal["spatial", "temporal"],
                      truncate_njoint: bool = False,
                      lower_bound: Union[None, float] = None,
                      upper_bound: Union[None, float] = None,
                      ) -> np.ndarray:
        """integrates the joint probability of data and prior over the prior under H0
        
        in more mathematical terms it integrates p(D,S|H0) over H0, where D stands for
        data, S for the prior of the sensory input and H0 for the separate causes
        hypothesis
        
        Parameters
        ----------
        observable : Observable
            one of the observable variables of the model
        sigma : float
            standard deviation of the spatial/temporal prior
        
        Returns
        -------
        result : ndarray
            said integral for every set of sensory input in the sample
        """
        
        mu_s = observable.sample
        sigma_s = observable.sigma
        # informed prior
        if dimension == "spatial":
            mu = observable.mu
        elif dimension == "temporal":
            mu = 0
        else:
            raise ValueError("non valid name for the dimension")
        
        # likelihoods' joint normalizing constant
        n_joint = 2 * np.pi * sigma * sigma_s
        if truncate_njoint:
            # "truncating" factor (i.e. trunactes n_joint)
            z = 0.5*(special.erf(
                        (upper_bound-mu)/
                        (np.sqrt(2)*sigma)) - 
                    special.erf(
                        (lower_bound-mu)/
                        (np.sqrt(2)*sigma)))
            n_joint *= z
        
        p_tot = 1 / sigma ** 2 + 1 / sigma_s ** 2
        mu_tilde = mu / sigma ** 2 + mu_s / sigma_s ** 2
        r = mu ** 2 / sigma ** 2 + mu_s ** 2 / sigma_s ** 2
        exponent = -(p_tot / 2) * (r / p_tot - (mu_tilde / p_tot) ** 2)
        sigma_tot = np.sqrt(1 / p_tot)
        result = 1 / n_joint * np.e ** (exponent) * np.sqrt(2 * np.pi) * sigma_tot
        
        if self.truncated:
            mu_tot = mu_tilde / p_tot
            z_numerator = 0.5*(special.erf(
                                (upper_bound-mu_tot)/
                                (np.sqrt(2)*sigma_tot)) - 
                               special.erf(
                                 (lower_bound-mu_tot)/
                                 (np.sqrt(2)*sigma_tot)))
            
            result *= z_numerator
        
        return result

    def __comIntegral(
        self,
        observable_real: Observable,
        observable_rub: Observable,
        sigma: float,
        dimension: Literal["spatial", "temporal"],
        truncate_njoint: bool = False,
        lower_bound: Union[None, float] = None,
        upper_bound: Union[None, float] = None,
    ) -> np.ndarray:
        """integrates the joint probability of data and prior over the prior under H0
        
        in more mathematical terms it integrates p(D,S|H0) over H0, where D stands for
        data, S for the prior of the sensory input and H0 for the separate causes
        hypothesis

        Parameters
        ----------
        observable_rub : Observable
            obervable parameters that originate from the rubber hand,
            i.e. the visual input
        observable_real : Observable
            observable parameters that are felt by the real hand, i.e. the
            proprioceptive and tactile input
        sigma : float
            standard deviation of the spatial/temporal prior

        Returns
        -------
        result : np.ndarray
            said integral for every set of sensory input in the sample
        """
        
        mu1 = observable_rub.sample
        sigma1 = observable_rub.sigma
        mu2 = observable_real.sample
        sigma2 = observable_real.sigma
        # informed prior
        if dimension == "spatial":
            mu = observable_rub.mu
        elif dimension == "temporal":
            mu = 0
        else:
            raise ValueError("non valid name for the dimension")
        
        # likelihoods' joint normalizing constant
        n_joint = (2 * np.pi) ** 1.5 * sigma * sigma1 * sigma2

        if truncate_njoint:
            # "truncating" factor (i.e. trunactes n_joint)
            z = 0.5*(special.erf(
                        (upper_bound-mu)/
                        (np.sqrt(2)*sigma)) -
                    special.erf(
                        (lower_bound-mu)/
                        (np.sqrt(2)*sigma)))
            n_joint *= z
        
        p_tot = 1 / sigma ** 2 + 1 / sigma1 ** 2 + 1 / sigma2 ** 2
        mu_tilde = mu / sigma ** 2 + mu1 / sigma1 ** 2 + mu2 / sigma2 ** 2
        r = mu ** 2 / sigma ** 2 + mu1 ** 2 / sigma1 ** 2 + mu2 ** 2 / sigma2 ** 2
        exponent = -(p_tot / 2) * (r / p_tot - (mu_tilde / p_tot) ** 2)
        sigma_tot = np.sqrt(1 / p_tot)
        result = 1 / n_joint * np.e ** exponent * np.sqrt(2 * np.pi) * sigma_tot
        
        if truncate_njoint:
            mu_tot = mu_tilde / p_tot
            z_numerator = 0.5*(special.erf(
                                (upper_bound-mu_tot)/
                                (np.sqrt(2)*sigma_tot)) -
                            special.erf(
                                (lower_bound-mu_tot)/
                                (np.sqrt(2)*sigma_tot)))
            result *= z_numerator

        return result

    def infer(self) -> None:
        """calculate posterior probability of each set of sensory input in the sample
        
        saves the means of said posterior probability in the attributes p0/p1 and the
        means of the likelihoods in like0/like1 as summary statistics
        
        Behavior under truncated model
        ------------------------------
        Under the common cause hypothesis sensory input cannot indicate a hand position
        outside of arm's reach. Therefore, the bounds for com_x (spatial prior under
        C_1) are set to the proprioceptive bounds
        
        Returns
        -------
        None
        """
        
        # if a seed was specified, set it
        if self.seed:
            np.random.set_state(self.seed)
        
        # draw samples to do inference on
        for param in self.params:
            param.draw_sample(self.n)

        # factors of the marginal likelihoods
        ind_xv = self.__sepIntegral(self.xv, self.sigma, "spatial",
                                    truncate_njoint = self.truncated,
                                    lower_bound = -self.bound_chiV,
                                    upper_bound = self.bound_chiV)
        ind_xp = self.__sepIntegral(self.xp, self.sigma, "spatial",
                                    truncate_njoint = self.truncated,
                                    lower_bound = -self.bound_chiP,
                                    upper_bound = self.bound_chiP)
        ind_tv = self.__sepIntegral(self.tv, self.sigma, "temporal",
                                    truncate_njoint = self.truncated,
                                    lower_bound = 0,
                                    upper_bound = self.bound_tau)
        ind_tt = self.__sepIntegral(self.tt, self.sigma, "temporal",
                                    truncate_njoint = self.truncated,
                                    lower_bound = 0,
                                    upper_bound = self.bound_tau)

        com_x = self.__comIntegral(self.xv, self.xp, self.sigma, "spatial",
                                   truncate_njoint = self.truncated,
                                   lower_bound = -self.bound_chiP,
                                   upper_bound = self.bound_chiP)
        com_t = self.__comIntegral(self.tv, self.tt, self.sigma, "temporal",
                                   truncate_njoint = self.truncated,
                                   lower_bound = 0,
                                   upper_bound = self.bound_tau)

        # higher level marginal likelihoods
        com = com_x * com_t
        ind = ind_xv * ind_xp * ind_tv * ind_tt
        
        # prior probability of C_1
        p = self.hyperprior

        marginal = com * p + ind * (1 - p)

        for c in [0, 1]: # 0 -> C_2; 1 -> C_1
            prior = p ** c * (1 - p) ** (1 - c)
            likelihood = com * c + ind * (1 - c)
            posterior = (likelihood * prior) / marginal
            # collapse array to a single value
            likelihood_mean = np.mean(likelihood)
            posterior_mean = np.mean(posterior)
            sem = stats.sem(posterior)
            if c == 0:
                self.like_c2 = likelihood_mean
                self.post_c2 = posterior_mean
                self.sem_c2 = sem
            else:
                self.like_c1 = likelihood_mean
                self.post_c1 = posterior_mean
                self.sem_c1 = sem


if __name__ == "__main__":
    # run model for testing purposes
    model = BCIBO_Model()
    aModel = BCIBO_Model(synch=False)
